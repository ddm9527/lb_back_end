package com.lb.mapper.redpacket;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.redpacket.Redpacket;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public interface RedpacketMapper extends BaseMapper<Redpacket> {

    @Select("SELECT gold_coin FROM lb_user WHERE id = #{user_id}")
    BigDecimal getGold_coin(@Param("user_id") Integer user_id);

    @Update("UPDATE lb_redpacket SET remain_num=remain_num-1, remain_money=remain_money+#{remain_money} where id=#{id}")
    Integer updateRemain(@Param("id") Integer id, @Param("remain_money") BigDecimal remain_money);

    @Update("UPDATE lb_redpacket SET best_userid=#{best_userid}, best_username=#{best_username}, best_money=#{best_money} where id=#{id}")
    Integer updateBest(@Param("id") Integer id, @Param("best_userid") Integer best_userid
            , @Param("best_username") String best_username, @Param("best_money") BigDecimal best_money);

    @Update("UPDATE lb_redpacket_get SET isbest=#{isbest} WHERE redpacket_id=#{id} AND get_userid=#{user_id}")
    Integer changeBest(@Param("id") Integer id, @Param("user_id") Integer user_id, @Param("isbest") Integer isbest);

    @Update("UPDATE lb_user SET gold_coin=gold_coin+(#{money}) WHERE id=#{user_id}")
    Integer changeGoldcoin(@Param("user_id") Integer user_id, @Param("money") BigDecimal money);

    @Update("UPDATE lb_redpacket set state=#{state} WHERE id=#{id}")
    Integer setState(@Param("id") Integer id, @Param("state") Integer state); // 设置红包状态

}
