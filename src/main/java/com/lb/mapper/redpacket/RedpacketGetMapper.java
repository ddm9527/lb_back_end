package com.lb.mapper.redpacket;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lb.model.redpacket.Resuit_redpacketget;
import com.lb.model.redpacket.Redpacket_get;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RedpacketGetMapper extends BaseMapper<Redpacket_get> {

    @Select("SELECT r.*,u.profile_picture as getUserprofilepicture FROM lb_redpacket_get r INNER JOIN user u WHERE r.get_userid=u.id AND r" +
            ".redpacket_id = #{redpacket_id} ORDER BY id")
    List<Resuit_redpacketget> selectList(Integer redpacket_id);

    @Select("SELECT * FROM lb_redpacket_get")
    IPage<Redpacket_get> page(Page page, QueryWrapper queryWrapper);

}
