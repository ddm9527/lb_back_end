package com.lb.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.user.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
}
