package com.lb.mapper.group;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lb.model.group.Group;
import com.lb.model.vo.UserGroup;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupMapper extends BaseMapper<Group> {



    @Select("SELECT b.group_id,b.user_id,b.nick,b.role,a.`name` as group_name,a.avatar FROM `lb_group` a INNER join lb_user_group b where b.group_id=a.id  and b.user_id=#{userId}")
    List<UserGroup> getListByUserId(Integer userId);

    @Select("SELECT b.group_id,b.user_id,b.nick,b.role,a.`name` as group_name,a.avatar FROM `lb_group` a INNER join lb_user_group b where b.group_id=a.id  and b.user_id=#{userId} and group_id=#{GroupId}")
    UserGroup getOneByUserIdAndGroupid(Integer userId,Integer GroupId);

    @Update("update lb_user_group set role=#{role} where user_id=#{memberId} and group_id=#{groupId}")
    Integer changeIdentity(Integer groupId, Integer memberId, Integer role);

    @Select("select count(role) from lb_user_group where group_id=#{groupId} and role=2")
    Integer countManager(Integer groupId);

    @Insert("insert into lb_user_group(user_id,group_id,nick,role) VALUES(#{userId},#{groupId},#{nick},#{role})")
    Integer saveUserGroup(UserGroup userGroup);

}
