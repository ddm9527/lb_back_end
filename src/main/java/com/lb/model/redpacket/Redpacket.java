package com.lb.model.redpacket;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("lb_redpacket")
public class Redpacket implements Serializable {

    @TableId(type = IdType.AUTO)
    Integer id; //红包id

    Integer userId; //发红包人id

    String userName; //发红包人姓名

    Integer type; //红包类型(预留:0、普通:1、拼手气:2)

    String content; //红包文本

    Integer totalNum; //红包总个数

    Integer remainNum; //红包剩余个数

    Date sendTime; //发放时间

    Date expireTime; //过期时间

    BigDecimal totalMoney; //总金额

    BigDecimal remainMoney; //剩余金额

    BigDecimal bestMoney; //手气最佳金额

    Integer bestUserid; //手气最佳领取人id

    String bestUsername; //手气最佳领取人姓名

    Integer state; //红包状态(0:待定、1:已领完、2:未领完、3:过期)

}
