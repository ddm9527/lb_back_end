package com.lb.model.redpacket;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class Resuit_redpacketget implements Serializable {

    @TableId(type = IdType.AUTO)
    Integer id; //id

    Integer redpacketId; //红包id

    Integer getUserid; //领取人id

    String getUsername; //领取人姓名

    Date getTime; //领取时间

    BigDecimal getMoney; //领取金额

    Integer isbest; //是否手气最佳

    String getUserprofilepicture; // 领取人头像

}
