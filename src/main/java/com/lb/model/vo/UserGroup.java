package com.lb.model.vo;

import com.lb.model.group.Group;
import com.lb.model.user.User;
import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserGroup implements Serializable {
    Integer userId;

    Integer groupId;

    String nick;

    Integer role;

    String groupName;

    String avatar;


}
