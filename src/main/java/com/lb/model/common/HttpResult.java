package com.lb.model.common;

import lombok.Data;

@Data
public class HttpResult {
    int code;
    String msg;
}
