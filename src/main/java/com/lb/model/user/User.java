package com.lb.model.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("lb_user")
public class User implements Serializable {
    @TableId(type = IdType.AUTO)
    Integer id;

    String accountName;

    String password;

    Integer phone;

    String name;

    Integer sex;

    Integer age;

    String profilePicture;

    Date createTime;

    Integer isForbidden;

    Date loginTime;

    Integer isCheckPhone;

    BigDecimal goldCoin;
}
