package com.lb.common;

import java.lang.reflect.Field;
import java.math.BigDecimal;

/**
 * 基本配置类：每个静态变量取值对应数据库表basesetting中的一条记录
 */
public class BasesettingProperties {
    public static long uploadpicsize = 524288; // 上传图片大小限制(bt) 默认512*1024
    public static int expiretime = 12; // 红包过期时间 默认12(小时)
    public static BigDecimal minmoney = new BigDecimal(0.1); // 拼手气红包最低金额
    // 以下添加其他变量...
    public static String othervariable;


    /**
     * 设置/改变Basesetting中静态变量值
     *
     * @param keyName 静态变量名
     * @param value   静态变量值
     * @return boolean
     */
    public static boolean set(String keyName, String value) {
        try {
            Class cls = Class.forName("com.lb.common.BasesettingProperties");
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals(keyName)) {
                    switch (keyName) {
                        case "uploadpicsize":
                            long sizevalue = Long.parseLong(value) * 1024;
                            field.set(field.get(cls), sizevalue);
                            break;
                        default:
                            field.set(field.get(cls), value);
                            break;
                    }
                    return true;
                }
            }
            return true;
        } catch (ClassNotFoundException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
    }
}
