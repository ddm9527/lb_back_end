package com.lb.util.task;

import com.lb.service.redpacket.RedpacketService;
import com.lb.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;

@Component
@Configurable
public class RedpacketTimedtask {

    @Autowired
    RedpacketService rpService;

    @Autowired
    RedisUtil redis;

    @Scheduled(cron = "0/30 * * * * ?")
    public void task() {
        Set<String> redpacketSet = redis.keys("redpacket:send*");
        for (String key : redpacketSet) {
            Map<String, Object> rp = (HashMap<String, Object>) redis.get(key);
            // 过期红包的处理
            if (new Date().compareTo((Date) rp.get("expire_time")) > 0) {
                outofdateManage(key, rp);
                System.out.println("过期红包清理完成");
            }
        }
    }

    // 处理过期红包
    private boolean outofdateManage(String key, Map<String, Object> rp) {
        String[] redisKey = key.split(":");
        String rp_id = redisKey[redisKey.length-1];
        Integer redpacket_id = Integer.parseInt(rp_id);
        // 返还金额
        rpService.changeGoldcoin((Integer) rp.get("user_id"), (BigDecimal) rp.get("remain_money"));
        // 标记过期
        rpService.setState(redpacket_id,3);
        // 清理redis缓存
        redis.del("redpacket:send:" + redpacket_id);
        redis.del("redpacket:tryopen:" + redpacket_id);
        return true;
    }
}
