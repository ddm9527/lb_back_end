package com.lb.util;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomUtil {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            BigDecimal[] moneys = mathRandom(BigDecimal.valueOf(8.88), 5, new BigDecimal(0.01));
//            BigDecimal[] moneys = mathGenearl(BigDecimal.valueOf(8.88), 5);
            if (moneys != null) {
                BigDecimal b = new BigDecimal(0);
                for (BigDecimal bigDecimal : moneys) {
                    System.out.print(bigDecimal + "元    ");
                    b = b.add(bigDecimal);
                }
                System.out.print("   总额：" + b + "元 ");
                System.out.println();
            }
        }
    }

    public static BigDecimal[] mathGenearl(BigDecimal money, int number) {
        BigDecimal[] mon = new BigDecimal[number];
        for (int i=0;i<number;i++) {
            mon[i] = money;
        }
        return mon;
    }

    /**
     * 计算每人获得红包金额
     *
     * @param total  红包总额
     * @param number 总人数
     * @param min    红包最小金额(0.01或0.1或1)
     * @return
     */
    public static BigDecimal[] mathRandom(BigDecimal total, int number, BigDecimal min) {
        if (total.compareTo(min.multiply(new BigDecimal(number))) == -1) {
            return null;
        }
        int multiply = multiply(min.doubleValue());
        Random random = new Random();
        // 金钱，按分计算 10块等于 1000分
        int money = total.multiply(BigDecimal.valueOf(multiply)).intValue();
        // 随机数总额
        double count = 0;
        // 每人获得随机点数
        double[] arrRandom = new double[number];
        // 每人获得钱数
        BigDecimal[] reMoney = new BigDecimal[number];
        List<BigDecimal> arrMoney = new ArrayList<BigDecimal>(number);
        // 循环人数 随机点
        for (int i = 0; i < arrRandom.length; i++) {
            int r = random.nextInt((number) * (multiply - 1)) + 1;
            count += r;
            arrRandom[i] = r;
        }
        // 计算每人拆红包获得金额
        int c = 0;
        for (int i = 0; i < arrRandom.length; i++) {
            // 每人获得随机数相加 计算每人占百分比
            Double x = new Double(arrRandom[i] / count);
            // 每人通过百分比获得金额
            int m = (int) Math.floor(x * money);
            // 如果获得 0 金额，则设置最小值 1分钱
            if (m == 0) {
                m = 1;
            }
            // 计算获得总额
            c += m;
            // 如果不是最后一个人则正常计算
            if (i < arrRandom.length - 1) {
                arrMoney.add(new BigDecimal(m).divide(new BigDecimal(multiply)));
            } else {
                // 如果是最后一个人，则把剩余的钱数给最后一个人
                arrMoney.add(new BigDecimal(money - c + m).divide(new BigDecimal(multiply)));
            }
        }
        // 随机打乱每人获得金额
        Collections.shuffle(arrMoney);
        for (int i = 0; i < arrMoney.size(); i++) {
            reMoney[i] = arrMoney.get(i);
        }
        return reMoney;
    }

    private static int multiply(Double balance) {
        int dcimalDigits = 0;
        String balanceStr = balance.toString();
        int indexOf = balanceStr.indexOf(".");
        if (indexOf > 0) {
            dcimalDigits = balanceStr.length() - 1 - indexOf;
        }
        int multiply = (int) Math.pow(10, dcimalDigits);
        return multiply;
    }

}
