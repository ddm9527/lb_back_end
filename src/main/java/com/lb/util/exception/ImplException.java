package com.lb.util.exception;

/**
 * @author jack
 */
public class ImplException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ImplException() {
        super();
    }

    public ImplException(String message) {
        super(message);
    }

    public ImplException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImplException(Throwable cause) {
        super(cause);
    }

    protected ImplException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
