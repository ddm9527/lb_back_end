package com.lb.util.common.response;


import lombok.Getter;

public class Message {

    /**
     * 操作id
     */
    @Getter
    private int cmd;

    /**
     * 模块id
     */
    @Getter
    private int module;

    /**
     * 流水号
     */
    @Getter
    private int sn;

    /**
     * 数据
     */
    @Getter
    private String data;


    public Message setCmd(int cmd) {
        this.cmd = cmd;
        return this;
    }


    public Message setModule(int module) {
        this.module = module;
        return this;
    }


    public Message setSn(int sn) {
        this.sn = sn;
        return this;
    }

    public Message setData(String data) {
        this.data = data;
        return this;
    }

    public static Message valueOf(int sn, int module, int cmd, String data) {
        return valueOf(sn, module, cmd).setData(data);
    }

    public static Message valueOf(int sn, int module, int cmd) {
        return new Message().setSn(sn).setModule(module).setCmd(cmd);
    }
}
