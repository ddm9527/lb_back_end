package com.lb.util.common.response;


import com.lb.util.common.errer.ErrerMsg;

public class ResponseObj {

    public static final int SUCCESS_CODE = 0;

    public static final String SUCCESS_MSG = "SUCCESS";

    private int code;

    private Object data;

    private String message;
    private String success_msg;


    public int getCode() {
        return code;
    }

    public ResponseObj setCode(int code) {
        this.code = code;
        return this;
    }


    public Object getData() {
        return data;
    }

    public ResponseObj setData(Object data) {
        this.data = data;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseObj setMessage(String message) {
        this.message = message;
        return this;
    }

    public static ResponseObj createResponse(int code, String msg) {
        return new ResponseObj().setCode(code).setMessage(msg);
    }

    public static ResponseObj createResponse(int code, String msg, Object data) {
        return new ResponseObj().setCode(code).setMessage(msg).setData(data);
    }

    public static ResponseObj createSuccessResponse(Object obj) {
        return createResponse(SUCCESS_CODE, SUCCESS_MSG).setData(obj);
    }

    public static ResponseObj createSuccessResponse(Object obj, String success_msg) {
        return createResponse(SUCCESS_CODE, success_msg).setData(obj);
    }

    public static ResponseObj createSuccessResponse() {
        return createResponse(SUCCESS_CODE, SUCCESS_MSG);
    }


    public static ResponseObj createErrResponse(ErrerMsg errerMsg) {
        return createResponse(errerMsg.getCode(), errerMsg.getMessage());
    }

}
