package com.lb.controller.file;

import com.alibaba.fastjson.JSON;
import com.lb.common.BasesettingProperties;
import com.lb.model.common.HttpResult;
import com.lb.service.file.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/pic")
public class FileController {

    @Autowired
    FileService picService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST) // 等价于 @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public HttpResult upload(HttpServletRequest request, MultipartFile file) {
        HttpResult result = new HttpResult();
        // 判断文件大小
        if (!file.isEmpty() && file.getSize() < BasesettingProperties.uploadpicsize) {
            // 上传图片服务器并返回图片url
            String picurl = picService.upload(file);
            if (picurl != null){
                result.setCode(0);
                result.setMsg(picurl);
            }else{
                result.setCode(-1);
                result.setMsg("上传失败");
            }
        } else {
            result.setCode(1);
            result.setMsg("文件大于" + (BasesettingProperties.uploadpicsize / 1024) + "KB");
        }
        return result;
    }
}
