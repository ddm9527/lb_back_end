package com.lb.controller.group;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lb.controller.criteria.GroupIdenttyCriteria;
import com.lb.model.group.Group;
import com.lb.model.user.User;
import com.lb.model.vo.UserGroup;
import com.lb.service.group.GroupService;
import com.lb.util.RedisUtil;
import com.lb.util.common.response.ResponseObj;
import com.lb.util.exception.ImplException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    GroupService groupService;


    @RequestMapping(value = "/updatememberidentity",method = RequestMethod.POST)
    public ResponseObj updateMemberIdentity(@RequestBody GroupIdenttyCriteria groupIdenttyCriteria){
        Subject subject = SecurityUtils.getSubject();
        User user= (User) subject.getPrincipal();
        UserGroup one=groupService.getOneByUserIdAndGroupid(user.getId(),groupIdenttyCriteria.getGroupId());

        if(!groupIdenttyCriteria.getIdentity().equals(2) && !groupIdenttyCriteria.getIdentity().equals(3)){
            throw new ImplException("身份参数只能输入2或3");
        }
        if(one==null){
            throw new ImplException("该用户没有此群");
        }
        if(one.getRole()!=1){
            throw new ImplException("该用户不是群主");
        }
        if(user.getId().equals(groupIdenttyCriteria.getMemberId())){
            throw new ImplException("该用户不能改变自己身份");
        }
        UserGroup member=groupService.getOneByUserIdAndGroupid(groupIdenttyCriteria.getMemberId(),groupIdenttyCriteria.getGroupId());
        if(member==null){
            throw new ImplException("该成员不在此群，或为游客");
        }
        if(groupIdenttyCriteria.getIdentity()==2 ){
            if(member.getRole().equals(2)){
                throw new ImplException("提拔的会员已是管理员");
            }
            Integer count=groupService.countManager(groupIdenttyCriteria.getGroupId());
           //限制管理员超过5位
            if(count>5){
                throw new ImplException("该群管理员已达到上限");
            }
        }

        Integer isOk=groupService.changeIdentity(groupIdenttyCriteria.getGroupId(),groupIdenttyCriteria.getMemberId(),groupIdenttyCriteria.getIdentity());
        //为了给chart通知做信息留存。
        redisUtil.set("User:"+user.getId()+":Group:ChangeManager:"+groupIdenttyCriteria.getGroupId(), JSON.toJSONString(groupIdenttyCriteria),1800);
        return ResponseObj.createSuccessResponse(isOk);
    }


}
