package com.lb.controller.criteria;

import lombok.Data;

@Data
public class RedpacketCriteria {

    private Integer user_id;    // 发红包人id

    private String user_name; // 发红包人姓名

    private Integer type;       // 红包类型(预留:0、普通:1、拼手气:2)

    private String content; // 红包文本

    private Integer total_num;  // 红包个数

    private String money;    // 红包金额(注意：红包类型为1普通时：总金额=红包金额*红包个数)

}
