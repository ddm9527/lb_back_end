package com.lb.controller.criteria;

import lombok.Data;

@Data
public class OpenRedpacketCriteria {

    private int redpacket_id;

    private Integer user_id;    // 抢红包人id

    private String user_name;    // 抢红包人名字

}
