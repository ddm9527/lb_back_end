package com.lb.controller.criteria;

import lombok.Data;

import java.util.Date;

@Data
public class QueryCriteria {

    private Integer id;        // 查询请求码(通用)

    private Integer type;       // 请求类型(暂不用

    private String startTime;   // 开始时间

    private String endTime;     // 结束时间

    private Integer offset;     // 获取的列表记录偏移量(页码)

    private Integer count;      // 列表的分页数量(共几页)

    private Integer pagesize = 8;      // 每个分页的记录数量(每页条数 默认为10)

}
