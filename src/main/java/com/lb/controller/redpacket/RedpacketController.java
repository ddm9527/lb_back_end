package com.lb.controller.redpacket;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lb.common.BasesettingProperties;
import com.lb.controller.criteria.OpenRedpacketCriteria;
import com.lb.controller.criteria.QueryCriteria;
import com.lb.controller.criteria.RedpacketCriteria;
import com.lb.mapper.redpacket.RedpacketGetMapper;
import com.lb.model.redpacket.Resuit_redpacketget;
import com.lb.model.redpacket.Redpacket;
import com.lb.model.redpacket.Redpacket_get;
import com.lb.service.redpacket.RedpacketService;
import com.lb.util.RandomUtil;
import com.lb.util.RedisUtil;
import com.lb.util.common.response.ResponseObj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/redpacket")
public class RedpacketController {

    @Autowired
    RedpacketService rpService;

    @Autowired
    RedpacketGetMapper rpgMapper;

    @Autowired
    RedisUtil redis;

    /**
     * 发红包
     *
     * @param rpCriteria
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ResponseObj send(@RequestBody RedpacketCriteria rpCriteria) throws Exception {
        ResponseObj response = verify(rpCriteria);
        if (response.getCode() < 0) { // 校验发红包数据
            return response;
        }
        Redpacket rp = new Redpacket();
        rp.setUserId(rpCriteria.getUser_id());             // 发红包人id
        rp.setUserName(rpCriteria.getUser_name());             // 发红包人id
        System.out.println("rpCriteria.getUser_name():"+rpCriteria.getUser_name());
        BigDecimal money = new BigDecimal(rpCriteria.getMoney());
        BigDecimal[] predistribution = new BigDecimal[rpCriteria.getTotal_num()]; // 多红包预分配金额
        if (1 == rpCriteria.getType()) { // 普通红包
            predistribution = RandomUtil.mathGenearl(money, rpCriteria.getTotal_num());
            money = money.multiply(new BigDecimal(rpCriteria.getTotal_num())); // 总金额 = 红包金额 * 总个数
        } else if (2 == rpCriteria.getType()) { // 拼手气
            predistribution = RandomUtil.mathRandom(money, rpCriteria.getTotal_num(), BasesettingProperties.minmoney);
        }
        rp.setTotalMoney(money);
//        // 这里为了减少数据库io操作，暂取消校验改由前端校验
        if (checkUsermoney(rp.getUserId(), money)) {
            return ResponseObj.createResponse(-5, "用户金币总数不足");
        }
        rp.setType(rpCriteria.getType());                   // 红包类型(预留:0、普通:1、拼手气:2)
        rp.setContent(rpCriteria.getContent());             // 红包文本
        rp.setTotalNum(rpCriteria.getTotal_num());         // 红包总个数
        rp.setRemainMoney(rp.getTotalMoney());            // 剩余金额 = 总金额
        rp.setRemainNum(rp.getTotalNum());                // 剩余个数 = 总个数
        Date date = new Date();
        rp.setSendTime(date);                              // 发放时间为当前时间
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int expiretime = BasesettingProperties.expiretime;  // 取得设置的过期时间间隔(12小时)
        cal.add(Calendar.HOUR_OF_DAY, expiretime);
        rp.setExpireTime(cal.getTime());                   // 过期时间为当前时间+过期时间间隔
        rp.setState(2);                                     // 红包状态(0:待定、1:已领完、2:未领完、3:过期)
//        System.out.println("存储对象为:"+rp.toString());
        int res = rpService.insert(rp); // 插入红包表
        if (res > 0) {
            List tryOpen = new ArrayList<Object>(res);              // 新建“试拆红包”缓存"表"
            redis.set("redpacket:tryopen:" + rp.getId(), tryOpen);
        }

        rpService.changeGoldcoin((Integer) rp.getUserId(), rp.getTotalMoney().negate()); // 用户数据中扣除金币金额
//        redis.set("redpacket:maxid", "" + rst); // 当前最大红包maxid
        Map<String, Object> getRp = new HashMap<String, Object>();
        getRp.put("type", rp.getType());
        getRp.put("user_id", rp.getUserId());
        getRp.put("user_name", rp.getUserName());
        getRp.put("expire_time", rp.getExpireTime());
        getRp.put("yet_num", 0);                        // 此红包已领个数
        getRp.put("total_num", rp.getTotalNum());       // 此红包总个数
        getRp.put("predistribution", predistribution);  // 此红包预分配金额
        getRp.put("remain_money", money);
        if (2 == rp.getType()) {
            getRp.put("best_userid", -1);                   // 手气最佳用户id
            getRp.put("best_username", "");                 // 手气最佳用户姓名
            getRp.put("best_money", new BigDecimal(0)); // 手气最佳金额
        }
        // 将新发红包数据存入redis缓存
        redis.set("redpacket:send:" + rp.getId(), getRp); // redis对象不用设置过期时间，有专门的定时任务进行清理
        return ResponseObj.createResponse(0, "发红包成功", rp);
    }

    /**
     * 抢红包
     *
     * @param orc
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/open", method = RequestMethod.POST)
    public ResponseObj open(@RequestBody OpenRedpacketCriteria orc) throws Exception {
        // 试拆红包：同一用户不可抢两次，红包剩余个数为零也失败
        int ret = this.tryopen(orc);
        System.out.println("试拆红包ret:" + ret);
        Map<String, Object> getRp = (Map<String, Object>) redis.get("redpacket:send:" + orc.getRedpacket_id());
        if (getRp == null) { // redis缓存中取不到该红包数据，证明已领取完或已过期
            return ResponseObj.createResponse(-1, "手慢了,红包派完了");
        }
        Integer type = (Integer) getRp.get("type");
        Integer lastBestUserid = (Integer) getRp.get("best_userid");        // 上次手气最佳用户id
//        String lastBestUsername = (String)getRp.get("best_username");         // 上次手气最佳用户名字
        BigDecimal lastBestMoney = (BigDecimal) getRp.get("best_money"); // 上次手气最佳金额
        int total_num = (Integer) getRp.get("total_num");
        if (ret == -3) {
            return ResponseObj.createResponse(-3, "发红包者不能抢自己发的红包"); // 20191003发红包者能抢自己发的红包
        } else if (ret == -2) {
            return ResponseObj.createResponse(-2, "同一用户同一红包只能抢一次");
        } else if (ret == 0) { // 抢到最后一个红包的处理
            redis.del("redpacket:tryopen:" + orc.getRedpacket_id());
            redis.del("redpacket:send:" + orc.getRedpacket_id());
            rpService.setState(orc.getRedpacket_id(), 1); // 设置红包已领取完
        }
        // 抢成功时：从缓存得到所抢金额，更新红包已领个数、手气最佳和金额，更新redis缓存数据，保存抢红包用户数据，
        BigDecimal[] predistribution = (BigDecimal[]) getRp.get("predistribution");
        BigDecimal money = predistribution[ret];                // 从redis缓存获取这个红包的金额
        BigDecimal remain_money = ((BigDecimal) getRp.get("remain_money")).subtract(money);
        getRp.put("remain_money", remain_money);
        Redpacket rp = new Redpacket();
        rp.setType(type);
        rp.setId(orc.getRedpacket_id());
        rp.setRemainMoney(money.negate()); // 剩余金额应减去的金额
//        System.out.println(rp.toString());
        Redpacket_get rpg = new Redpacket_get();
        rpg.setRedpacketId(orc.getRedpacket_id());
        rpg.setGetUserid(orc.getUser_id());
        rpg.setGetUsername(orc.getUser_name());
        rpg.setGetMoney(money);
        rpg.setGetTime(new Date());
        if (type == 2 && money.compareTo(lastBestMoney) == 1) { // 如果金额大于手气最佳
            getRp.replace("best_userid", orc.getUser_id());     // 更新手气最佳者
            getRp.replace("best_username", orc.getUser_name()); // 更新手气最佳者
            getRp.replace("best_money", money);                 // 更新手气最佳金额
            rp.setUserId(lastBestUserid); // ！！特殊处理：这里临时用来保存上次手气最佳用户id
            rp.setBestUserid(orc.getUser_id());
            rp.setBestUsername(orc.getUser_name());
            rp.setBestMoney(money);
            rpg.setIsbest(1);
        }
        if (ret != 0) { // 不是最后一个红包则将更新的数据存入redis缓存
            redis.set("redpacket:send:" + orc.getRedpacket_id(), getRp);
        }
        new Thread() {
            public void run() {
                //子线程去干自己的事情,主线程不等待
                rpService.databaseProc(rp, rpg); // 数据库处理
            }
        }.start();
        return ResponseObj.createResponse(1, "恭喜你，抢到了红包", rpg);
    }

    // 试拆红包：同一用户不可抢两次，红包剩余个数为零也失败
    private synchronized Integer tryopen(OpenRedpacketCriteria orc) {
        Map<String, Object> getRp = (Map<String, Object>) redis.get("redpacket:send:" + orc.getRedpacket_id());
        if (getRp == null) { // redis缓存中取不到该红包数据，证明已领取完或已过期
            return -1;
        }else { // 20191003发红包者能抢自己发的红包
//            Integer user_id = (Integer) getRp.get("user_id");
//            if (orc.getUser_id() == user_id){
//                return -3; // 发红包人不能抢自己的红包
//            }
        }
        int yet_num = (Integer) getRp.get("yet_num");
        int total_num = (Integer) getRp.get("total_num");
        yet_num++;
        if (yet_num == total_num) {
            yet_num = 0; // 抢到了最后一个红包
        } else if (yet_num > total_num) {
            return -1; // 红包已抢完了
        }
        List<Integer> tryOpen = (List<Integer>) redis.get("redpacket:tryopen:" + orc.getRedpacket_id());
        if (tryOpen == null || tryOpen.size() == 0) { // 第一个抢红包的人
            tryOpen = new ArrayList<Integer>();
        } else {
            for (Integer user_id : tryOpen) {
                if (orc.getUser_id() == user_id) {
                    return -2; // 同一用户同一红包只能抢一次
                }
            }
        }
        tryOpen.add(orc.getUser_id());
        redis.set("redpacket:tryopen:" + orc.getRedpacket_id(), tryOpen);
        getRp.replace("yet_num", yet_num);      // 更新已领个数
        redis.set("redpacket:send:" + orc.getRedpacket_id(), getRp);
        return yet_num; // 返回是第几个抢成功的人
    }

    /**
     * 红包和领取详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/{id}")
    public ResponseObj show(@PathVariable Integer id) throws Exception {
        Redpacket redpacket = rpService.selectById(id);
        List<Resuit_redpacketget> redpacket_get = rpgMapper.selectList(id);
        Map<String, Object> data = new HashMap<>();
        data.put("redpacket", redpacket);
        data.put("redpacket_gets", redpacket_get);
        return ResponseObj.createResponse(0, "红包和领取详情", data);
    }

    @RequestMapping(value = "/usermoney/{user_id}")
    public ResponseObj usermoney(@PathVariable int user_id) throws Exception {
        BigDecimal money = rpService.getGold_coin(user_id);
        if (money != null) {
            return ResponseObj.createResponse(0, money.toString());
        } else {
            return ResponseObj.createResponse(-1, "无此用户");
        }
    }

    private Boolean checkUsermoney(Integer user_id, BigDecimal money) {
        BigDecimal gold_coin = rpService.getGold_coin(user_id); // 取得用户金币总额
        // 如果红包总金额>用户金币总数
        if (money.compareTo(gold_coin) == 1) {
            return true;
        }
        return false;
    }

    /**
     * 用户抢得的红包列表
     * @param query
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/userget", method = RequestMethod.POST)
    public ResponseObj userGet(@RequestBody QueryCriteria query) throws Exception {
        System.out.println("query:"+query);
        Page page = new Page();
        if (query.getOffset() != null){
            page.setCurrent(query.getOffset());
        }
        if (query.getPagesize() != null){
            page.setSize(query.getPagesize());
        }
        IPage<Redpacket_get> list = rpService.userGet(page, query);
        return ResponseObj.createResponse(0, "查询成功", list);
    }

    /**
     * 用户发出的红包列表
     * @param query
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/usersend", method = RequestMethod.POST)
    public ResponseObj userSend(@RequestBody QueryCriteria query) throws Exception {
        System.out.println("query:"+query);
        Page page = new Page();
        if (query.getOffset() != null){
            page.setCurrent(query.getOffset());
        }
        if (query.getPagesize() != null){
            page.setSize(query.getPagesize());
        }
        IPage<Redpacket> list = rpService.userSend(page, query);
        return ResponseObj.createResponse(0, "查询成功", list);
    }

    // 校验发红包数据
    private ResponseObj verify(RedpacketCriteria rp) {
        if (rp.getUser_id() == null || rp.getType() == null || rp.getTotal_num() == null || rp.getMoney() == null ||
                rp.getContent() == null) {
            return ResponseObj.createResponse(-1, "数据格式不正确");
        } else if (rp.getTotal_num() < 1) {
            return ResponseObj.createResponse(-2, "红包个数至少应为1");
        } else if (Double.parseDouble(rp.getMoney()) < rp.getTotal_num() * BasesettingProperties.minmoney.doubleValue()) {
            return ResponseObj.createResponse(-3, "红包金额太小了");
        } else if ("".equals(rp.getContent())) {
            return ResponseObj.createResponse(-4, "写几句祝福语吧");
        }
        return ResponseObj.createResponse(0, "校验成功");
    }
}
