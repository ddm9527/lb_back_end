package com.lb.controller.user;


import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.lb.config.shiro.MySessionManager;
import com.lb.controller.criteria.LoginCriteria;
import com.lb.controller.criteria.UserSettingCriteria;
import com.lb.model.user.User;
import com.lb.model.user.ViewUser;
import com.lb.model.vo.UserGroup;
import com.lb.service.group.GroupService;
import com.lb.service.user.UserService;
import com.lb.util.Md5Util;
import com.lb.util.RedisUtil;
import com.lb.util.common.response.ResponseObj;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.View;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.apache.shiro.subject.support.DefaultSubjectContext.PRINCIPALS_SESSION_KEY;

@RestController
public class UserController {


    @Autowired
    UserService userService;

    @Autowired
    GroupService groupService;

    @Autowired
    RedisUtil redisUtil;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseObj login(@RequestBody LoginCriteria loginCriteria) throws Exception {
        if (StrUtil.hasBlank(loginCriteria.getAccount()) || StrUtil.hasBlank(loginCriteria.getPassWord())) {
            return ResponseObj.createResponse(-1, "账号或者密码不能为空");
        }
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(new UsernamePasswordToken(loginCriteria.getAccount(), loginCriteria.getPassWord()));
        } catch (UnknownAccountException unknownAccountException) {
            return ResponseObj.createResponse(-1, "账号不存在");
        } catch (IncorrectCredentialsException i) {
            return ResponseObj.createResponse(-1, "账号或密码错误");
        } catch (ExcessiveAttemptsException e) {
            return ResponseObj.createResponse(-1, "密码错误输入次数过多，该账号禁止三分钟");
        }
        Map<String, Object> map = new HashMap<>();
        User user = (User) subject.getPrincipal();
        user.setPassword(null);
        //防止重复登录
        String usertoken = (String) redisUtil.get("usertoken:"+loginCriteria.getAccount());
        if (!StrUtil.hasBlank(usertoken) && !usertoken.equals(subject.getSession().getId().toString())) {
            try {
                redisUtil.del("shiro_redis_session:"+usertoken);
            } catch (UnknownSessionException u) {
                //空token是因为redis里的过期了，不影响
            }
        }
        redisUtil.set("usertoken:"+loginCriteria.getAccount(), subject.getSession().getId(), 604800);
        map.put("token", subject.getSession().getId());
        map.put("user", user);
        return ResponseObj.createSuccessResponse(map);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseObj logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return ResponseObj.createSuccessResponse("登出成功");
    }

    @RequestMapping(value = "/unauth", method = RequestMethod.GET)
    public ResponseObj unauth() {
        return ResponseObj.createResponse(-1, "未登录");
    }


    @RequestMapping(value = "/userregister", method = RequestMethod.POST)
    @Transactional
    public ResponseObj userRegister(@RequestBody LoginCriteria loginCriteria){
        User user = new User();
        user.setAccountName(loginCriteria.getAccount());
        user.setPassword(loginCriteria.getPassWord());
        user.setName(loginCriteria.getName());
        userService.userRegister(user);
        UserGroup userGroup=new UserGroup();
        userGroup.setUserId(user.getId());
        userGroup.setGroupId(1);
        userGroup.setNick(loginCriteria.getName());
        userGroup.setRole(3);
        groupService.saveUserGroup(userGroup);
       return ResponseObj.createSuccessResponse();
    };



    @RequestMapping(value = "/usersettings",method = RequestMethod.POST)
    public ResponseObj userSettings(@RequestBody UserSettingCriteria userSettingCriteria){
        Subject subject = SecurityUtils.getSubject();
        User user= (User) subject.getPrincipal();
        user = userService.userSettings(user.getId(),userSettingCriteria);
        PrincipalCollection principals = subject.getPrincipals();
        String realName= principals.getRealmNames().iterator().next();
        PrincipalCollection newPrincipalCollection = new SimplePrincipalCollection(user, realName);
        subject.runAs(newPrincipalCollection);
        return ResponseObj.createSuccessResponse();
    }

    @RequestMapping(value = "/viewinformation",method = RequestMethod.GET)
    public ResponseObj viewInformation(){
        Subject subject = SecurityUtils.getSubject();
        User user= (User) subject.getPrincipal();
        ViewUser viewUser=new ViewUser(user);
        return ResponseObj.createSuccessResponse(viewUser);
    }
    @RequestMapping(value = "/changepassword",method = RequestMethod.POST)
    public ResponseObj changePassword(@RequestParam String newpassword) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getPrincipal();
        User entity = userService.getById(user.getId());
        entity.setPassword(Md5Util.getMd5(newpassword));
        userService.updateById(entity);
        return ResponseObj.createSuccessResponse();
    };
}
