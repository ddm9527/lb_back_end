package com.lb;

import com.lb.common.BasesettingProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServerRunner implements CommandLineRunner {


    @Override
    public void run(String... args) throws Exception {
        initBasesetting(); // 启动后加载数据库中的“基本配置”
    }

    public void initBasesetting(){
        BasesettingProperties.set("uploadpicsize","1024");
    }

}
