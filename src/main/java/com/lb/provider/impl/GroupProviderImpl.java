package com.lb.provider.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.lb.model.group.Group;
import com.lb.model.vo.UserGroup;
import com.lb.provider.GroupProvider;
import com.lb.service.group.GroupService;
import com.lb.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@Service(interfaceClass = GroupProvider.class,timeout = 2000,version = "1.0.0")
public class GroupProviderImpl implements GroupProvider {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    GroupService groupService;

    @Override
    public String listGroupByUserId(Integer userid) {
        List<UserGroup> list=groupService.getListByUserId(userid);
        if(list.size()==0){
            UserGroup userGroup=new UserGroup();
            userGroup.setGroupId(1);
            userGroup.setGroupName("测试房");
            userGroup.setAvatar("http:22");
            userGroup.setRole(4);
            list.add(userGroup);
        }
        return JSON.toJSONString(list);
    }

    @Override
    public String updateMemberIdentity(Integer userId,Integer groupid) {
        String json= (String) redisUtil.get("User:"+userId+":Group:ChangeManager:"+groupid);
        if(json==null){
            return null;
        }
        redisUtil.del("User:"+userId+":Group:ChangeManager:"+groupid);
        return json;
    }
}
