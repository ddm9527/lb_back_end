package com.lb.provider.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.lb.config.shiro.MySessionManager;
import com.lb.model.user.User;
import com.lb.provider.UserProvider;
import com.lb.service.user.UserService;
import com.lb.util.UserUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.apache.shiro.subject.support.DefaultSubjectContext.PRINCIPALS_SESSION_KEY;

@Component
@Service(interfaceClass = UserProvider.class,timeout = 2000,version = "1.0.0")
public class UserProviderImpl implements UserProvider {

    static AtomicInteger visitorid=new AtomicInteger(80000);

    @Autowired
    UserUtil userUtil;

    @Autowired
    UserService userService;

    @Override
        public User checkLogin(String token) {
        User user=userUtil.getUserBytoken(token);
        if (user==null){
            user=new User();
            user.setId(visitorid.addAndGet(1));
            user.setName("游客:"+visitorid.get());
            user.setAge(0);
            user.setSex(0);
        }
        return user;
    }

    @Override
    public User flushUser(Integer id) {
        User user=userService.getById(id);
        user.setPassword(null);
        return user;
    }
}

