package com.lb.service.redpacket;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lb.controller.criteria.QueryCriteria;
import com.lb.model.redpacket.Redpacket;
import com.lb.model.redpacket.Redpacket_get;

import java.math.BigDecimal;
import java.util.List;

public interface RedpacketService {

    //获取用户金币总数
    BigDecimal getGold_coin(Integer user_id);

    Integer insert(Redpacket redpacket);

    Integer databaseProc(Redpacket rp, Redpacket_get rpg);

    // 更新用户金币额
    Integer changeGoldcoin(Integer user_id, BigDecimal money);

    // 标记红包过期
    Integer setState(Integer id,Integer state);

    // 通过redpacket_id查询红包信息
    Redpacket selectById(Integer id);

    // 查询用户抢得的红包列表
    IPage<Redpacket_get> userGet(Page page, QueryCriteria query);

    // 查询用户发出的红包列表
    IPage<Redpacket> userSend(Page page, QueryCriteria query);

}
