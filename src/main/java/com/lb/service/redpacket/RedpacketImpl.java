package com.lb.service.redpacket;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.controller.criteria.QueryCriteria;
import com.lb.mapper.redpacket.RedpacketGetMapper;
import com.lb.mapper.redpacket.RedpacketMapper;
import com.lb.model.redpacket.Redpacket;
import com.lb.model.redpacket.Redpacket_get;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class RedpacketImpl extends ServiceImpl<RedpacketMapper, Redpacket> implements RedpacketService {

    @Autowired
    RedpacketMapper rpMapper;

    @Autowired
    RedpacketGetMapper rpgMapper;

    //获取用户金币额
    @Override
    public BigDecimal getGold_coin(Integer user_id) {
        return this.baseMapper.getGold_coin(user_id);
    }

    @Override
    public Integer insert(Redpacket rp) {
        int res = this.baseMapper.insert(rp);
        return rp.getId();
    }

    @Override
    @Transactional
    public Integer databaseProc(Redpacket rp, Redpacket_get rpg) {
        try {
            // 更新此红包的：剩余个数、剩余金额
            this.baseMapper.updateRemain(rp.getId(), rp.getRemainMoney());
            if (rp.getUserId() != null && rp.getType() == 2) { //更新手气最佳
                this.baseMapper.updateBest(rp.getId(), rp.getBestUserid(), rp.getBestUsername(), rp.getBestMoney());
                if (0 < rp.getUserId()) {
                    this.baseMapper.changeBest(rp.getId(), rp.getUserId(), 0); // 上次手气最佳改回普通
                }
            }
            // 领红包表新增一条数据
            rpgMapper.insert(rpg);
            // 更新领红包人用户的金币总额
            this.baseMapper.changeGoldcoin(rpg.getGetUserid(), rpg.getGetMoney());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    @Override
    public Integer changeGoldcoin(Integer user_id, BigDecimal money) {
        return this.baseMapper.changeGoldcoin(user_id, money);
    }

    @Override
    public Integer setState(Integer id, Integer state) {
        return this.baseMapper.setState(id, state);
    }

    @Override
    public Redpacket selectById(Integer id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public IPage<Redpacket_get> userGet(Page page, QueryCriteria param) {
        QueryWrapper<Redpacket_get> query = new QueryWrapper<>();
        if (param.getId() == null){
            return null;
        }
        query.eq("get_userid", param.getId());
        if (param.getStartTime() != null && param.getEndTime() != null) {
            query.and(qw -> qw.apply("unix_timestamp(get_time) BETWEEN unix_timestamp('" + param.getStartTime() + "') AND unix_timestamp('" + param.getEndTime() + "')"));
        }else if (param.getStartTime() != null && param.getEndTime() == null){
            query.and(qw -> qw.apply("unix_timestamp(get_time) >= unix_timestamp('" + param.getStartTime() + "')"));
        }else if (param.getStartTime() == null && param.getEndTime() != null){
            query.and(qw -> qw.apply("unix_timestamp(get_time) <= unix_timestamp('" + param.getEndTime() + "')"));
        }
        query.orderByDesc("id");
        return rpgMapper.page(page, query);
    }

    @Override
    public IPage<Redpacket> userSend(Page page, QueryCriteria query) {
        QueryWrapper<Redpacket> queryWrapper = new QueryWrapper<>();
        if (query.getId() == null){
            return null;
        }
        queryWrapper.eq("user_id", query.getId());
        if (query.getStartTime() != null && query.getEndTime() != null) {
            queryWrapper.and(qw -> qw.apply("unix_timestamp(send_time) BETWEEN unix_timestamp('" + query.getStartTime() + "') AND unix_timestamp('" + query.getEndTime() + "')"));
        }else if (query.getStartTime() != null && query.getEndTime() == null){
            queryWrapper.and(qw -> qw.apply("unix_timestamp(send_time) >= unix_timestamp('" + query.getStartTime() + "')"));
        }else if (query.getStartTime() == null && query.getEndTime() != null){
            queryWrapper.and(qw -> qw.apply("unix_timestamp(send_time) <= unix_timestamp('" + query.getEndTime() + "')"));
        }
        queryWrapper.orderByDesc("id");
        return rpMapper.selectPage(page, queryWrapper);
    }
}
