package com.lb.service.file;

import com.lb.util.FtpUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Calendar;
import java.util.Random;

@Service
public class FileServiceImpl implements FileService {

    @Value("${ftp.basepath}")
    private String FTP_BASE_PATH;
    @Value("${ftp.address}")
    private String FTP_ADDRESS;
    @Value("${ftp.port}")
    private Integer FTP_PORT;
    @Value("${ftp.username}")
    private String FTP_USERNAME;
    @Value("${ftp.password}")
    private String FTP_PASSWORD;
    @Value("${ftp.image_base_url}")
    private String IMAGE_BASE_URL;

    @Override
    public String upload(MultipartFile file) {
        // 上传文件功能实现
        try {
            String path = savePicture(file);
            String showurl = IMAGE_BASE_URL + path;
//            System.out.println("上传结果showurl:"+showurl);
            // 数据回显url
            return showurl;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String savePicture(MultipartFile uploadFile) throws Exception {
        // 判断文件是否为空,如果为空则返回
        if (uploadFile.isEmpty()) { return null ; }
        // 设置文件上传的目录,以日期为单位,提高访问速度
        Calendar now = Calendar.getInstance();
        String filePath = "/" + now.get(Calendar.YEAR) + "/" + String.format("%02d", (now.get(Calendar.MONTH) + 1)) + "/"
                + String.format("%02d", now.get(Calendar.DAY_OF_MONTH));
//        System.out.println("filePath" + filePath);
        // 获取原始文件名
        String originalFilename = uploadFile.getOriginalFilename();
//        System.out.println("originalFilename:" + originalFilename);
        // 生成新文件名
        String newFileName = genImageName()
                + originalFilename.substring(originalFilename.lastIndexOf("."));
//        System.out.println("newFileName:" + newFileName);
        // 上传文件
        boolean b = FtpUtil.uploadFile(FTP_ADDRESS, FTP_PORT, FTP_USERNAME, FTP_PASSWORD,
                FTP_BASE_PATH, filePath, newFileName, uploadFile.getInputStream());
//        System.out.println("上传结果:" + b +", res:" + filePath + "/" + newFileName);
        return filePath + "/" + newFileName;
    }

    /**
     * 图片名生成
     */
    public static String genImageName() {
        // 取当前时间的长整形值包含毫秒
        long millis = System.currentTimeMillis();
        // long millis = System.nanoTime();
        // 加上三位随机数
        Random random = new Random();
        int end3 = random.nextInt(999);
        // 如果不足三位前面补0
        String str = millis + String.format("%03d", end3);
//        str += UUID.randomUUID() + str;
        return str;
    }
}
