package com.lb.service.file;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface FileService {
    String upload(MultipartFile file);
}
