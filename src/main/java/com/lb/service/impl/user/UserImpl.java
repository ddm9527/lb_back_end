package com.lb.service.impl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.controller.criteria.UserSettingCriteria;
import com.lb.mapper.user.UserMapper;
import com.lb.model.user.User;
import com.lb.service.user.UserService;
import com.lb.util.Md5Util;
import com.lb.util.exception.ImplException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class UserImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    public User findByUsername(String userName) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("account_name", userName);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public User userRegister(User user) {
        User hasUser = baseMapper.selectOne(new QueryWrapper<User>().eq("account_name", user.getAccountName()));
        if (hasUser != null) {
            throw new ImplException("该账号已被注册");
        }
        if (null == user.getAccountName() || null == user.getPassword()) {
            throw new ImplException("用户名和密码不能为空！");
        }
        Date now = new Date();
        user.setCreateTime(now);
        user.setSex(0);
        user.setAge(0);
        user.setIsForbidden(0);
        user.setGoldCoin(new BigDecimal(0));
        user.setPassword(Md5Util.getMd5(user.getPassword()));
        if (1 != baseMapper.insert(user)) {
            throw new ImplException("插入数据库发生未知错误！");
        }

        return user;
    }

    @Override
    public User userSettings(Integer id,UserSettingCriteria setting) {
        User user=baseMapper.selectById(id);
        if (setting.getName() != null) {
            user.setName(setting.getName());
        }
        if(setting.getAge()!=null){
            user.setAge(setting.getAge());
        }
        if(setting.getSex()!=null){
            user.setSex(setting.getSex());
        }
        if(setting.getProfilePicture()!=null){
            user.setProfilePicture(setting.getProfilePicture());
        }
        baseMapper.updateById(user);
        return user;
    }
}
