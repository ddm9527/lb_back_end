package com.lb.service.impl.group;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.mapper.group.GroupMapper;
import com.lb.model.group.Group;
import com.lb.model.vo.UserGroup;
import com.lb.service.group.GroupService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements GroupService {


    @Override
    public List<UserGroup> getListByUserId(Integer userId) {
        return this.baseMapper.getListByUserId(userId);
    }

    @Override
    public UserGroup getOneByUserIdAndGroupid(Integer userId, Integer groupId) {
        return this.baseMapper.getOneByUserIdAndGroupid(userId,groupId);
    }

    @Override
    public Integer changeIdentity(Integer groupId, Integer memberId, Integer role) {
        return this.baseMapper.changeIdentity(groupId,memberId,role);
    }

    @Override
    public Integer countManager(Integer goupId) {
        return this.baseMapper.countManager(goupId);
    }

    @Override
    public void saveUserGroup(UserGroup userGroup) {

       this.baseMapper.saveUserGroup(userGroup);

    }
}
