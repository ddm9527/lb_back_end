package com.lb.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.controller.criteria.UserSettingCriteria;
import com.lb.model.user.User;

public interface UserService extends IService<User> {

    User findByUsername(String userName);

    User userRegister(User user);

    User userSettings(Integer id, UserSettingCriteria setting);

}
